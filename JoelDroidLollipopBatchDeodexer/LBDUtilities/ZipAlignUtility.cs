﻿using JoelDroidLollipopBatchDeodexer.LBDObjects;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace JoelDroidLollipopBatchDeodexer.LBDUtilities
{
    public class ZipAlignUtility
    {
        public static bool ZipAlignApk(string ApkName)
        {
            bool zipalignStatus = false;
            FileInfo fiZipALign = new FileInfo(ToolPaths.ZIPALIGN_PATH);
            FileInfo fiWorkingApk = new FileInfo(string.Format(ToolPaths.WORKING_APK_PATH, ApkName));
            FileInfo fiWorkingJar = new FileInfo(string.Format(ToolPaths.WORKING_JAR_PATH, ApkName));

            if (fiWorkingApk.Exists)
            {
                DirectoryInfo diZipalign = new DirectoryInfo(fiWorkingApk.Directory.FullName + @"\Zipaligned");
                if (!diZipalign.Exists)
                {
                    diZipalign.Create();
                }

                if (fiZipALign.Exists && fiWorkingApk.Exists)
                {
                    string commandString = string.Format(" -f -v 4 {0} {1}", fiWorkingApk.FullName, diZipalign.FullName + @"\" + fiWorkingApk.Name);

                    ProcessInput processIn = new ProcessInput();
                    processIn.FileName = fiZipALign.FullName;
                    processIn.Arguments = commandString;
                    processIn.WorkingDirectory = fiWorkingApk.Directory.FullName; ;

                    ProcessOutput processOut = CommonUtility.RunProcess(processIn);
                    try
                    {
                        FileInfo zaApk = new FileInfo(diZipalign.FullName + @"\" + fiWorkingApk.Name);
                        if (zaApk.Exists)
                        {
                            File.Delete(fiWorkingApk.FullName);
                            zaApk.MoveTo(string.Format(ToolPaths.WORKING_APK_PATH, ApkName));

                            zipalignStatus = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        zipalignStatus = false;
                        Logger.LogException(ex.Message, ex.StackTrace);
                    }
                }
            }
            else if(fiWorkingJar.Exists)
            {
                zipalignStatus = true;
            }
            return zipalignStatus;
        }
    }
}
