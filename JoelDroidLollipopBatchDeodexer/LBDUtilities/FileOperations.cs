﻿using System;
using System.Collections.Generic;
using System.IO;

namespace JoelDroidLollipopBatchDeodexer.LBDUtilities
{
    public class FileOperations
    {
        private static List<FileInfo> preDeOdexedList = new List<FileInfo>();

        #region Copy Operations

        public static bool CopyBootOat(FileInfo fiBootOat)
        {
            bool isCopySuccess = false;
            if (fiBootOat.Exists)
            {
                try
                {
                    DirectoryInfo diBoot = new DirectoryInfo(ToolPaths.WORKING_PATH + @"\boot");
                    if (diBoot.Exists)
                    {
                        diBoot.Delete(true);
                    }
                    diBoot.Create();
                    fiBootOat.CopyTo(ToolPaths.BOOT_OAT_PATH);

                    FileInfo fiBoot = new FileInfo(ToolPaths.BOOT_OAT_PATH);
                    isCopySuccess = fiBoot.Exists;
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex.Message, ex.StackTrace);
                }
            }

            return isCopySuccess;
        }

        public static bool CopyToWorkingDirectory(DirectoryInfo source)
        {
            bool copyStatus = false;
            DirectoryInfo target = new DirectoryInfo(ToolPaths.WORKING_PATH + @"\" + source.Name);

            if (source.Exists)
            {
                try
                {
                    FileInfo[] odexFiles = CommonUtility.GetFilesByExtensions(source, true, "*.apk", "*.odex.xz", "*.odex");

                    if (odexFiles.Length == 2)
                    {
                        if (!target.Exists)
                        {
                            target.Create();
                        }
                        odexFiles[0].CopyTo(target + @"\" + odexFiles[0].Name);
                        odexFiles[1].CopyTo(target + @"\" + odexFiles[1].Name);

                        FileInfo fiCopy1 = new FileInfo(target + @"\" + odexFiles[0].Name);
                        FileInfo fiCopy2 = new FileInfo(target + @"\" + odexFiles[0].Name);

                        copyStatus = fiCopy1.Exists && fiCopy2.Exists;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex.Message, ex.StackTrace);
                }
                
            }
            return copyStatus;
        }

        public static bool CopyFrameworkAppToWorkingDirectory(FileInfo[] frameworkApp)
        {
            bool odexStatus = false;

            DirectoryInfo target = new DirectoryInfo(ToolPaths.WORKING_PATH + @"\" + frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4)));
            try
            {
                if (!target.Exists)
                {
                    target.Create();
                }

                frameworkApp[0].CopyTo(target.FullName + @"\" + frameworkApp[0].Name);
                frameworkApp[1].CopyTo(target.FullName + @"\" + frameworkApp[1].Name);
                odexStatus = true;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex.Message, ex.StackTrace);
            }
            return odexStatus;
        }

        #endregion

        #region Get Apps

        public static List<DirectoryInfo> GetAllApps(DirectoryInfo ParentDir)
        {
            List<DirectoryInfo> apps = new List<DirectoryInfo>();
            if (ParentDir.Exists)
            {
                DirectoryInfo[] diApps = ParentDir.GetDirectories();
                foreach (DirectoryInfo diApp in diApps)
                {
                    FileInfo fiOdexXZ;
                    FileInfo fiOdex;

                    if (GlobalVariables.isRom64Bit)
                    {
                        fiOdexXZ = new FileInfo(diApp.FullName + @"\arm64\" + diApp.Name + ".odex.xz");
                        fiOdex = new FileInfo(diApp.FullName + @"\arm64\" + diApp.Name + ".odex");
                    }
                    else
                    {
                        fiOdexXZ = new FileInfo(diApp.FullName + @"\arm\" + diApp.Name + ".odex.xz");
                        fiOdex = new FileInfo(diApp.FullName + @"\arm\" + diApp.Name + ".odex");
                    }

                    if (fiOdexXZ.Exists || fiOdex.Exists)
                    {
                        apps.Add(diApp);
                    }
                    else
                    {
                        FileInfo[] preDeOdexedApp = CommonUtility.GetFilesByExtensions(diApp, false, "*.apk");
                        if(preDeOdexedApp.Length ==1)
                        {
                            preDeOdexedList.Add(preDeOdexedApp[0]);
                        }
                    }
                }
            }
            return apps;
        }

        public static List<FileInfo[]> GetAllFrameworkApps(DirectoryInfo frameworkDirectory)
        {
            List<FileInfo[]> frameworkFilePairs = new List<FileInfo[]>();
            if (frameworkDirectory.Exists)
            {
                DirectoryInfo diArm;
                if (GlobalVariables.isRom64Bit)
                {
                    diArm = new DirectoryInfo(frameworkDirectory.FullName + @"\arm64");
                }
                else
                {
                    diArm = new DirectoryInfo(frameworkDirectory.FullName + @"\arm");
                }
                
                if (diArm.Exists)
                {
                    FileInfo[] armOdexFiles = diArm.GetFiles("*.odex*");

                    if (armOdexFiles != null && armOdexFiles.Length > 0)
                    {
                        foreach (FileInfo armOdexFile in armOdexFiles)
                        {
                            string odexFileName_WithoutExtension = string.Empty;
                            if (armOdexFile.Name.Contains(".odex.xz"))
                            {
                                odexFileName_WithoutExtension = armOdexFile.Name.Substring(0, (armOdexFile.Name.Length - 8));
                            }
                            else
                            {
                                odexFileName_WithoutExtension = armOdexFile.Name.Substring(0, (armOdexFile.Name.Length - 5));
                            }
                            FileInfo jarFile = new FileInfo(frameworkDirectory.FullName + @"\" + odexFileName_WithoutExtension + ".jar");
                            FileInfo apkFile = new FileInfo(frameworkDirectory.FullName + @"\" + odexFileName_WithoutExtension + ".apk");

                            if (jarFile.Exists)
                            {
                                FileInfo[] frameworkFilePair = new FileInfo[2] { jarFile, armOdexFile };
                                frameworkFilePairs.Add(frameworkFilePair);
                            }
                            else if (apkFile.Exists)
                            {
                                FileInfo[] frameworkFilePair = new FileInfo[2] { apkFile, armOdexFile };
                                frameworkFilePairs.Add(frameworkFilePair);
                            }
                        }
                    }
                }
            }


            return frameworkFilePairs;
        }

        public static List<FileInfo[]> GetAllBootFrameworkApps(DirectoryInfo frameworkDirectory)
        {
            List<FileInfo[]> frameworkFilePairs = new List<FileInfo[]>();
            if (frameworkDirectory.Exists)
            {
                DirectoryInfo diBootArm = new DirectoryInfo(ToolPaths.BOOT_OAT_DEX_PATH);
                if (diBootArm.Exists)
                {
                    FileInfo[] bootArmOdexFiles = diBootArm.GetFiles("*.dex");

                    if (bootArmOdexFiles != null && bootArmOdexFiles.Length > 0)
                    {
                        foreach (FileInfo bootArmOdexFile in bootArmOdexFiles)
                        {
                            string odexFileName_WithoutExtension = bootArmOdexFile.Name.Substring(0, (bootArmOdexFile.Name.Length - 4));
                            
                            FileInfo jarFile = new FileInfo(frameworkDirectory.FullName + @"\" + odexFileName_WithoutExtension + ".jar");

                            if (jarFile.Exists)
                            {
                                FileInfo[] frameworkFilePair = new FileInfo[2] { jarFile, bootArmOdexFile };
                                frameworkFilePairs.Add(frameworkFilePair);
                            }
                        }
                    }
                }
            }


            return frameworkFilePairs;
        }
        
        #endregion

        #region Move Operations

        public static bool MoveDeodexedApp(DirectoryInfo destAppDir)
        {
            bool moveStatus = false;
            FileInfo fiApk = new FileInfo(string.Format(ToolPaths.WORKING_APK_PATH, destAppDir.Name));
            if (fiApk.Exists && destAppDir.Exists)
            {
                string apkPath = destAppDir.FullName + @"\" + fiApk.Name;
                string armFolderPath = string.Empty;
                if (GlobalVariables.isRom64Bit)
                {
                    armFolderPath = destAppDir.FullName + @"\arm64";
                }
                else
                {
                    armFolderPath = destAppDir.FullName + @"\arm";
                }

                try
                {
                    FileInfo fiTargetApk = new FileInfo(apkPath);
                    if (fiTargetApk.Exists)
                    {
                        fiTargetApk.Delete();
                    }
                    fiApk.MoveTo(apkPath);
                    Directory.Delete(armFolderPath, true);
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex.Message, ex.StackTrace);
                }

                FileInfo fiMoved = new FileInfo(apkPath);
                moveStatus = fiMoved.Exists;
            }
            return moveStatus;
        }

        public static bool MoveDeodexedFrameworkApp(FileInfo[] appPair, string appName)
        {
            bool moveStatus = false;
            FileInfo fiJar = new FileInfo(string.Format(ToolPaths.WORKING_JAR_PATH, appName));
            if (fiJar.Exists && appPair[0].Exists && appPair[1].Exists)
            {
                try
                {
                    appPair[0].Delete();
                    appPair[1].Delete();

                    fiJar.MoveTo(appPair[0].FullName);
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex.Message, ex.StackTrace);
                }

                FileInfo fiMoved = new FileInfo(appPair[0].FullName);
                moveStatus = fiMoved.Exists;
            }
            return moveStatus;
        }

        #endregion

        #region Rename Operations

        public static bool RenameFile(string appName)
        {
            bool isRenameSuccess = false;
            try
            {
                FileInfo tempDexOut = new FileInfo(string.Format(ToolPaths.TEMP_DEX_PATH, appName));
                if (tempDexOut.Exists)
                {
                    File.Move(tempDexOut.FullName, string.Format(ToolPaths.CLASSES_DEX_PATH, appName));
                }

                isRenameSuccess = File.Exists(string.Format(ToolPaths.CLASSES_DEX_PATH, appName));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex.Message, ex.StackTrace);
            }
            return isRenameSuccess;
        }

        public static bool RenameClasses2(string appName)
        {
            bool isRenameSuccess = false;
            try
            {
                FileInfo tempDexOut = new FileInfo(string.Format(ToolPaths.FRAMEWORK_CLASSES2_DEX_PATH, appName));
                if (tempDexOut.Exists)
                {
                    File.Move(tempDexOut.FullName, string.Format(ToolPaths.CLASSES2_DEX_PATH, appName));
                }

                isRenameSuccess = File.Exists(string.Format(ToolPaths.CLASSES2_DEX_PATH, appName));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex.Message, ex.StackTrace);
            }
            return isRenameSuccess;
        }

        #endregion

        public static bool CleanWorkingFolderAppDirectory(string appDirName)
        {
            DirectoryInfo diAppClean = new DirectoryInfo(ToolPaths.WORKING_PATH + @"\" + appDirName);
            if (diAppClean.Exists)
            {
                try
                {
                    diAppClean.Delete(true);
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex.Message, ex.StackTrace);
                }
            }

            DirectoryInfo diAppCleanCheck = new DirectoryInfo(ToolPaths.WORKING_PATH + @"\" + appDirName);
            
            return !diAppCleanCheck.Exists;
        }

        public static void CleanFrameworkArmFolders(DirectoryInfo di_framework)
        {
            if (di_framework.Exists)
            {
                DirectoryInfo di_arm = new DirectoryInfo(di_framework.FullName + @"\arm");
                if (di_arm.Exists)
                {
                    di_arm.Delete(true);
                }
                di_arm = new DirectoryInfo(di_framework.FullName + @"\arm64");
                if (di_arm.Exists)
                {
                    di_arm.Delete(true);
                }
            }
        }
    }
}
