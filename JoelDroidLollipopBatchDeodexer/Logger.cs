﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoelDroidLollipopBatchDeodexer
{
    public class Logger
    {
        private static List<string> sbLog;

        public static void Log(string logEntry)
        {
            if (sbLog == null)
            {
                sbLog = new List<string>();
            }

            if(!string.IsNullOrEmpty(logEntry))
            {
                sbLog.Add(getTime(true) + logEntry);
            }
        }

        public static void LogException(string exception, string stackTrace)
        {
            if (sbLog == null)
            {
                sbLog = new List<string>();
            }

            sbLog.Add(getTime(true) + "EXCEPTION: " + exception + ", ST: " + stackTrace);
        }

        public static void WriteLog()
        {
            string deskTopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (sbLog != null && sbLog.Count >0)
            {
                string fileName = "DeodexLog - " + getTime(false) + ".txt";
                File.WriteAllLines(deskTopPath + @"\" + fileName, sbLog);
            }
        }
        private static string getTime(bool isLogEntry)
        {
            string dateString = string.Empty;
            DateTime dtNow = DateTime.Now;
            if (isLogEntry)
            {
                dateString = dtNow.ToString("dd-MM-yyyy:HH-mm-ss => ");
            }
            else
            {
                dateString = dtNow.ToString("dd-MM-yyyy HH-mm-ss");
            }
            return dateString;
        }
    }
}
